package com.portafolio.feriavirtual.controller;

import com.portafolio.feriavirtual.exception.ResourceNotFoundException;
import com.portafolio.feriavirtual.model.Rol;
import com.portafolio.feriavirtual.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class RolController {

    @Autowired
    private RolRepository rolRepository;

    @GetMapping("/roles")
    public Page<Rol> getAllPosts(Pageable pageable) {
        return rolRepository.findAll(pageable);
    }

    @PostMapping("/roles")
    public com.portafolio.feriavirtual.model.Rol createPost(@Valid @RequestBody com.portafolio.feriavirtual.model.Rol rol) {
        return rolRepository.save(rol);
    }

    @PutMapping("/roles/{rolId}")

    public com.portafolio.feriavirtual.model.Rol updatePost(@PathVariable long rolId, @Valid @RequestBody com.portafolio.feriavirtual.model.Rol rolRequest) {

        return rolRepository.findById(rolId).map(rol -> {
            rol.setNombre(rolRequest.getNombre());
            return rolRepository.save(rol);
        }).orElseThrow(() ->new ResourceNotFoundException("RolId" + rolId + " not found"))  ;
    }

    @DeleteMapping("/roles/{rolId}")
    public ResponseEntity<?> deleteRol(@PathVariable Long rolId) {
        return rolRepository.findById(rolId).map(post -> {
            rolRepository.delete(post);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("PostId " + rolId + " not found"));
    }

}
