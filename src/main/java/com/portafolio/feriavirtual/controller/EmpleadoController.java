package com.portafolio.feriavirtual.controller;

import com.portafolio.feriavirtual.exception.ResourceNotFoundException;
import com.portafolio.feriavirtual.model.Empleado;
import com.portafolio.feriavirtual.repository.EmpleadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EmpleadoController {

    @Autowired
    private EmpleadoRepository empleadoRepository;

    @GetMapping("/empleados")
    public Page<Empleado> getAllEmpleados(Pageable pageable) {
        return empleadoRepository.findAll(pageable);
    }

    @PostMapping("/empleados")
    public com.portafolio.feriavirtual.model.Empleado createPost(@Valid @RequestBody com.portafolio.feriavirtual.model.Empleado empleado) {
        return empleadoRepository.save(empleado);
    }

    @PutMapping("/empleados/{empleadoId}")
    public com.portafolio.feriavirtual.model.Empleado updatePost(@PathVariable long empleadoId, @Valid @RequestBody com.portafolio.feriavirtual.model.Empleado empleadoRequest) {

        return empleadoRepository.findById(empleadoId).map(empleado -> {
            empleado.setId(empleadoRequest.getId());
            empleado.setNombre(empleadoRequest.getNombre());
            empleado.setApellido(empleadoRequest.getApellido());
            empleado.setFechaNacimiento(empleadoRequest.getFechaNacimiento());
            empleado.setEmail(empleadoRequest.getEmail());
            return empleadoRepository.save(empleado);
        }).orElseThrow(() ->new ResourceNotFoundException("EmpleadoId" + empleadoId + " not found"))  ;
    }

    @DeleteMapping("/empleados/{empleadoId}")
    public ResponseEntity<?> deleteRol(@PathVariable Long empleadoId) {
        return empleadoRepository.findById(empleadoId).map(post -> {
            empleadoRepository.delete(post);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("EmpleadoId " + empleadoId + " not found"));
    }
}
